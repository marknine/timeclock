# Timeclock #

Timeclock is a PHP driven Time-Management software for individuals or businesses that require this function. There is support for Employees, Freelance users and Administrative users. More functions are being added as time comes.

### Features ###

Employees:
* 'Punching' to clock in and clock out.
* Static schedules
* Adding notes per punch
* Statistics per pay period
* Export time sheets

Freelance Users:
* Edit punches
* Insert/Correct earnings per working day

Administrative Users:
* See who is working and manually punch in/out any user.
* See who's scheduled to work and manually edit schedules or user settings

